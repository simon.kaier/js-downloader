import { readFileSync } from 'fs'
import { mkdir, rm, stat } from 'fs/promises'
import path from 'path'
import { parseM3U8 } from '../m3u8/m3u8-parser'
import { downloadM3U8 } from './downloader'

jest.setTimeout(1000 * 60)
describe('downloader', () => {
  const m3u8 = parseM3U8(
    readFileSync(path.resolve('src/test-assets/test.m3u8')).toString()
  )
  const outDir = path.resolve('test-out')
  beforeEach(async () => {
    await mkdir(outDir, { recursive: true })
  })
  afterEach(async () => {
    await rm(outDir, { recursive: true })
  })
  it('should download the test m3u8', async () => {
    const testFileName = path.join(outDir, 'test.mp4')
    let prevProgress = -1
    await downloadM3U8(m3u8, testFileName, (progress) => {
      expect(progress).toBeGreaterThan(prevProgress)
      prevProgress = progress
    })

    const statsWritten = await stat(testFileName)
    expect(statsWritten.size).toBeGreaterThan(200_000_000)
    expect(statsWritten.size).toBeLessThan(400_000_000)
    expect(statsWritten).toBeTruthy()
  })
})
