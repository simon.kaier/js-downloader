import { M3U8Content } from '../m3u8/m3u8-parser'
import axios from 'axios'
import { Semaphore } from 'await-semaphore'
import { WriteFile } from '../write-file'
import { rm } from 'fs/promises'
import { MinDelay } from '../utils'

const MAX_RETRIES = 5
const NUMBER_CONCURRENT_DOWNLOADS = 20

const minDelayLock = new MinDelay(450)

export async function downloadM3U8(
  m3u8: M3U8Content,
  destination: string,
  onProgress: (progress: number) => void = () => {}
): Promise<{ failed: boolean }> {
  const semaphore = new Semaphore(NUMBER_CONCURRENT_DOWNLOADS)
  const writer = new WriteFile(destination)

  let countDownloaded = 0
  let prevReportedProgress = -1
  let failed = false
  let i = 0
  for (const url of m3u8) {
    if (failed) {
      break
    }
    const release = await semaphore.acquire()
    const index = i
    downloadPart(url)
      .then(async (buffer) => {
        await writer.addPart({
          content: buffer,
          release,
          index,
        })

        countDownloaded++
        const percentFinished = Math.round(
          (100 * countDownloaded) / m3u8.length
        )
        if (prevReportedProgress !== percentFinished) {
          onProgress(percentFinished)
          prevReportedProgress = percentFinished
        }
      })
      .catch((err) => {
        console.error(
          `Failed to download part ${i} for ${destination}. Abort download.`,
          err
        )
        failed = true
      })

    i++
  }

  // drain running downloads before closing
  const releases = await Promise.all(
    new Array(NUMBER_CONCURRENT_DOWNLOADS)
      .fill(1)
      .map(() => semaphore.acquire())
  )
  writer.close()
  for (const release of releases) {
    release()
  }

  if (failed) {
    console.warn(`Remove failed download of: ${destination}`)
    await rm(destination, { recursive: false })
  }

  return { failed }
}

async function downloadPart(
  url: string,
  currentRetry = 0
): Promise<ArrayBuffer> {
  try {
    // wait at least n ms before starting another download
    await minDelayLock.waitForSlot()
    const resp = await axios(url, {
      responseType: 'arraybuffer',
    })

    return resp.data
  } catch (e) {
    if (currentRetry < MAX_RETRIES) {
      // exponential fade out delay
      const delaySecondsHalf = 2 ** currentRetry
      console.info(
        `Retry ${currentRetry + 1} to ${
          delaySecondsHalf / 2
        }s delay download part ${url}`
      )
      await new Promise((res) => setTimeout(res, 500 * delaySecondsHalf))
      return downloadPart(url, currentRetry + 1)
    } else {
      console.warn(`Failed to download part ${url}`, e)
      throw e
    }
  }
}
