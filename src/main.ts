import axios, {AxiosResponse} from 'axios'
import { inspect } from 'util'
import { mkdir, rm, writeFile } from 'fs/promises'
import {createWriteStream} from 'fs';
import { config } from './config'
import { downloadM3U8 } from './downloader/downloader'
import { parseM3U8 } from './m3u8/m3u8-parser'
import path from 'path'
import { leftPad } from './utils'
import {parse} from 'node-html-parser'
import { checkFfmpegExists, copyVideoFile } from './ffmpeg/ffmpeg'
import { IncomingMessage } from 'http'

const START = 1028
const END = 1063

const id = Math.round(Math.random() * 10000)

const secChHeaders = {
  'sec-ch-ua':
    '".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}

const secFetchCorsHeaders = {
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-origin'
}

let exampleFileNumber = 0

async function writeDebugFile(fileName: string, content: string | Buffer) {
  if (config.WRITE_INTERMEDIATE_CRAWLS) {
    const fName =
      './example-files/' + id + '_' + exampleFileNumber + '_' + fileName
    await writeFile(fName, content)
  }
}

const visitOnePieceTube = async (opTubeUrl: string): Promise<string> => {
  const resp = await axios({
    url: opTubeUrl
  })
  const body = resp.data as string
  const indexIframe = body.indexOf('var S1 =\'<iframe')
  const srcStart = body.indexOf('src="', indexIframe) + 'src="'.length
  const srcEnd = body.indexOf('"', srcStart)
  await writeDebugFile('one-piece-tube.html', body)

  return body.substring(srcStart, srcEnd)
}

const visitAnividzV2 = async (anividzUrl: string) => {
  const goUrl = anividzUrl + '&do=getVideo'
  // wanted:
  // https://anividz.org/player/index.php?data=daecf755df5b1d637033bb29b319c39a&do=getVideo
  // actual:
  // https://anividz.org/player/index.php?data=daecf755df5b1d637033bb29b319c39a&do=getVideo
  const reqUrl = new URL(anividzUrl)
  const urlSearchParams = reqUrl.searchParams
  const resp = await axios.post(
    goUrl,
    {
      hash: urlSearchParams.get('data'),
      r: 'https://onepiece-tube.com/'
    },
    {
      method: 'POST',
      headers: {
        referer: anividzUrl,
        ...secChHeaders,
        ...secFetchCorsHeaders,
        'x-requested-with': 'XMLHttpRequest',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }
  )

  const body = resp.data as {
    videoSource: string
    securedLink: string
  }

  await writeDebugFile('anividzDo.json', JSON.stringify(body, null, 2))
  return body
}

const getSourceList = async (anividzSecureUrl: string, referer: string) => {
  console.log('Fetch source list:', anividzSecureUrl)

  const resp = await axios.get(anividzSecureUrl, {
    headers: {
      referer,
      ...secChHeaders,
      ...secFetchCorsHeaders
    }
  })

  const body = resp.data as string
  /* Example response:
          #EXTM3U
          #EXT-X-VERSION:3
          ## [ FirePlayer ] by Neron (c) 2018~ | firevideoplayer.com | Skype : neronsilence ##
          #EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720
          https://anividz.org/hls/NSt4V0F5d29mYTBBY0Y3bnpxdVNYcERUUVhENVRVd09HcmxhSmJ0dE1KSG5RbGpxZWs5WTA0NlkveTd0VlVUZFNYU0VtcVRab2MrWEtZMHNYZERQZUxFQWdkTG9nTkdrRDdzUnJrVEJJWWtvTU80NVBrUzJkelo1NzZwSGlGdlpEVkhhOFV5QWdrWkN2VzZEWFE5Z2ZtTUVQOHNVT3VGc1hTSERHWWhGb2pGVFd4MGhiM1p4Tk96OXFNcWVEWDZ6Znhla1hPdHBrWWRiY2xxeDVMUGVsSDZKTmNlQWhML1hmOXdUa3pYcnp5aWpidnZabTd5UHB0akFrZE9XZExKWWpQYkFoVTFXcGlNdjJDbmRLVGV1VHRUR3AyZUcwZkdpbEwxekYrdnZ4VXBocWtyVE9LWkUwM1YyMzAvV2pSQWY%3D
       */
  const lines = body.split('\n')
  const url = lines[lines.length - 1]

  await writeDebugFile('anividzSrcList.txt', body)

  return url
}

const getM3U8 = async (m3u8Url: string, referer: string) => {
  console.log('Fetch m3u8:', m3u8Url)
  const resp = await axios.get(m3u8Url, {
    headers: {
      referer,
      ...secChHeaders,
      ...secFetchCorsHeaders
    }
  })

  const body = resp.data as string
  await writeDebugFile('list.m3u8', body)
  return body
}

async function downloadFromAnividz(anividzUrl: string, episode: number, outFileName: string) {
  const srcIntermediate = await visitAnividzV2(anividzUrl)
  const srcList = await getSourceList(
    srcIntermediate.videoSource ?? srcIntermediate.securedLink,
    anividzUrl
  )
  const m3u8List = await getM3U8(srcList, anividzUrl)

  await mkdir(config.FILES_OUT_DIR, { recursive: true })
  console.log('Download episode ' + episode + ' to ' + outFileName)
  return await downloadM3U8(
    parseM3U8(m3u8List),
    outFileName,
    (progress) => {
      if (config.LOG_DOWNLOAD_PROGRESS) {
        console.info(`Download ${progress}% of E${episode}`)
      }
    }
  )
}

async function downloadFromHubu(anividzUrl: string, episode: number, outFileName: string): Promise<{
  failed: boolean
}> {
  const resp = await axios({
    url: anividzUrl
  })
  const body = resp.data as string
  const document = parse(body)
  const sourceElement = document.querySelector('video > source')
  const srcUrl = sourceElement?.getAttribute('src')
  if (!srcUrl) {
    console.error(`failed to find video url in body: ${body}`)
    return {
      failed: true
    }
  }

  const videoResponse: AxiosResponse<IncomingMessage> = await axios({
    url: srcUrl,
    responseType: 'stream',
  })
  if (videoResponse.status == 200) {
    const finished = new Promise(resolve => {
      videoResponse.data.on('close', resolve)
      videoResponse.data.on('finish', resolve)
    })
    videoResponse.data.pipe(createWriteStream(outFileName))

    await finished
  } else {
    console.error("Failed to download video from hubu:", videoResponse)
  }

  return {
    failed: false
  }
}

const download = async (episode: number) => {
  const episodeStr = leftPad(episode.toString(), '0', 4)
  const videoHosterUrl = await visitOnePieceTube(
    `http://onepiece-tube.com/folge/${episode}`
  )

  const outFileName = path.resolve(config.FILES_OUT_DIR, `E__${episodeStr}.mp4`)
  let downloadResult
  if (videoHosterUrl.startsWith('https://hubu.cloud')) {
    console.log('Download from hub.cloud')
    downloadResult = await downloadFromHubu(videoHosterUrl, episode, outFileName)
  } else {
    downloadResult = await downloadFromAnividz(videoHosterUrl, episode, outFileName)
  }

  if (
    config.FFMPEG_TRANSCODE &&
    !downloadResult.failed &&
    checkFfmpegExists()
  ) {
    const copyName = path.resolve(config.FILES_OUT_DIR, `E${episodeStr}.mp4`)
    await copyVideoFile(outFileName, copyName)
    await rm(outFileName, { recursive: false })
  }
}

async function run() {
  for (let episode = START; episode <= END; episode++) {
    console.log('start download episode', episode)
    await download(episode)
    console.log('finish download episode', episode)
  }
}

run()
  .then(() => console.log('finished'))
  .catch((err) => console.error(inspect(err)))
