import { leftPad } from './utils'

describe('leftPad', () => {
  it('should leave long enought strings untouched', () => {
    expect(leftPad('ABC', '0', 3)).toBe('ABC')
    expect(leftPad('ABC', '0', 1)).toBe('ABC')
    expect(leftPad('', '0', 0)).toBe('')
  })

  it('should add left padding', () => {
    expect(leftPad('', '0', 4)).toBe('0000')
    expect(leftPad('1', '0', 4)).toBe('0001')
    expect(leftPad('11', '0', 4)).toBe('0011')
    expect(leftPad('111', '0', 4)).toBe('0111')
    expect(leftPad('1111', '0', 4)).toBe('1111')
  })

  it('should throw on incorrect length', () => {
    expect(() => leftPad('', 'to long', 1)).toThrow()

    // too short
    expect(() => leftPad('a', '', 0)).toThrow()
  })
})
