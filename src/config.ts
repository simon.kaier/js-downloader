export const config = {
  LOG_DOWNLOAD_PROGRESS: true,
  WRITE_INTERMEDIATE_CRAWLS: true,
  FILES_OUT_DIR: 'downloads',
  FFMPEG_TRANSCODE: true,
} as const
