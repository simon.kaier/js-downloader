import assert from 'assert'

export type M3U8Content = string[]

export function parseM3U8(m3u8Raw?: string): M3U8Content {
  assert(
    typeof m3u8Raw === 'string' && m3u8Raw.length > 0,
    'Input must be a non-empty string'
  )
  const result = m3u8Raw
    .split('\n')
    .filter((line) => line.length > 0 && line[0] !== '#')

  assert.ok(result.length > 0, 'Result must have more then one url')

  return result
}
