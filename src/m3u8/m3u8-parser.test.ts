import { readFileSync } from 'fs'
import * as path from 'path'
import { parseM3U8 } from './m3u8-parser'

describe('parseM3U8', () => {
  const m3u8TestFileContent = readFileSync(
    path.resolve('src/test-assets/test.m3u8')
  ).toString()

  it('should load testFile', () => {
    expect(m3u8TestFileContent.length).toBeGreaterThan(100)
    expect(m3u8TestFileContent.indexOf('#EXTM3U')).toBe(0)
    expect(m3u8TestFileContent.indexOf('NOT FOUND')).toBe(-1)
  })

  it('should parse a m3u8 File', () => {
    const parseResult = parseM3U8(m3u8TestFileContent)
    expect(Array.isArray(parseResult)).toBe(true)
    expect(parseResult.length).toBe(277)

    for (const link of parseResult) {
      expect(link).toMatch(/^https.*/)
    }
  })
})
