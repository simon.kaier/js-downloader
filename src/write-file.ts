import { createWriteStream, WriteStream } from 'fs'

export interface Part {
  index: number
  content: ArrayBuffer
  release: () => void
}

export class WriteFile {
  readonly fileStream: WriteStream
  isFinished: boolean = false

  private readonly queue = new Array<Part>()
  private lastWrittenPart = -1

  constructor(readonly fileName: string) {
    this.fileStream = createWriteStream(fileName)
  }

  async addPart(part: Part) {
    this.queue.push(part)

    await this.checkQueue()
  }

  private async checkQueue() {
    while (this.queue.length) {
      const nextPartIndex = this.lastWrittenPart + 1
      const partInQueue = this.queue.find(
        (item) => item.index === nextPartIndex
      )
      if (partInQueue) {
        await this.pushPart(partInQueue)
        partInQueue.release()
      } else {
        break
      }
    }
  }

  private pushPart(part: Part) {
    this.lastWrittenPart = part.index
    return new Promise<void>((resolve, reject) => {
      this.fileStream.write(part.content, (error) => {
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }

  finish() {
    this.isFinished = true
  }

  close() {
    this.fileStream.end()
  }
}
