import { checkFfmpegExists, copyVideoFile } from './ffmpeg'
import path from 'path'
import { rm, stat } from 'fs/promises'

describe('ffmpeg', () => {
  it('should report that ffmpeg exists', () => {
    expect(checkFfmpegExists()).toBe(true)
  })

  const testSrcPath = path.resolve('src/test-assets/test-video.mp4')
  const testDestPath = path.resolve('src/test-assets/test-video-copy.mp4')

  afterAll(async () => {
    await rm(testDestPath, { recursive: false })
  })

  it('should copy a test asset', async () => {
    await copyVideoFile(testSrcPath, testDestPath)
    const stats = await stat(testDestPath)
    expect(stats.size).toBe(443158)
  })
})
