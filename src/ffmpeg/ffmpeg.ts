/**
 * Uses ffmpeg copy to fix the container of the video file
 * @param path the source of the video file
 * @param dest the destination on where to copy
 */
import { stat } from 'fs/promises'
import assert from 'assert'
import { spawn, spawnSync } from 'child_process'

let ffmpegExists: boolean | null = null
export function checkFfmpegExists() {
  if (ffmpegExists === null) {
    const child = spawnSync('ffmpeg', ['-version'], { encoding: 'utf-8' })
    const output = child.output.filter((line) => !!line)
    ffmpegExists =
      output.length > 0 && output[0]!.indexOf('ffmpeg version ') === 0
  }
  return ffmpegExists
}

export async function copyVideoFile(src: string, dest: string): Promise<void> {
  const stats = await stat(src)
  assert(stats.size > 10_000, 'Video file seems to small ' + src)

  // ffmpeg -i E957.mp4 -c copy OP_E957.mp4
  const child = spawn('ffmpeg', ['-i', src, '-c', 'copy', dest, '-metadata', 'episode'])

  return new Promise<void>((resolve, reject) => {
    child.on('exit', () => {
      console.log('child process exited')
    })
    child.on('close', () => {
      console.log('child process closed')
      resolve()
    })
    child.on('error', (err) => {
      console.error('ffmpeg errored out', err)
      reject(err)
    })
  })
}
