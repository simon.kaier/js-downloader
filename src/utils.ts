import assert from 'assert'
import { Semaphore } from 'await-semaphore'

export function leftPad(
  input: string,
  padding: string,
  minLength: number
): string {
  assert(padding.length === 1, 'Padding string must be one character!')
  if (input.length >= minLength) return input

  const paddingWidth = minLength - input.length
  return padding.repeat(paddingWidth) + input
}

export class MinDelay {
  private lock = new Semaphore(1)
  constructor(private readonly minDelayMs: number) {}

  async waitForSlot() {
    const release = await this.lock.acquire()
    setTimeout(() => release(), this.minDelayMs)
  }
}
